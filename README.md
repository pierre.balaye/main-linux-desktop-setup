# Main linux desktop setup

```
sudo apt update && sudo apt upgrade -y
sudo apt install -y git-all vim
echo 'nnoremap <F3> :.w !bash<CR>' > ~/.vimrc
git config --global credentials.helper store
git clone https://gitlab.univ-lille.fr/pierre.balaye/main-linux-desktop-setup.git ~/main-linux-desktop-setup
```



