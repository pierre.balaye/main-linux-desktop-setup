source ./installers/source-to-set-repo-dir-var.sh
./installers/smart-caps-lock-installer.sh
./installers/top-setup.sh
./installers/git-setup.sh
./installers/vim-setup.sh
./installers/command-line-tools.sh
./installers/tmux-setup.sh
./installers/terminator-install.sh
./installers/oh-my-zsh.sh
./installers/copy-zsh-files-to-custom-dir.sh
./installers/home-bin-commands.sh
./installers/autokey-setup.sh
./installers/R-setup.sh
./installers/zotero-setup.sh
./installers/nextcloud-setup.sh
